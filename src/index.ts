import { Client } from '@nerdvision/gitlab-js';
import json2md from 'json2md'

import dotenv from 'dotenv'
dotenv.config()
const fs = require('fs');
import { Command } from 'commander';
import { posix } from 'node:path';
const program = new Command();

program
  .option('-g, --group <type>', 'specify group')
program.parse(process.argv);
const options = program.opts();
interface ProjectCIStatus {
  name: string
  branch: string
  status: string
}
const token = process.env.BOT_TOKEN
console.log(token); //Just to show that it is masked

const client = new Client({
  token: token
});

let getProjectCollection = async (groupName: string) => {
  const group = await client.groups.find(groupName);
  const projects = await group.projects.list();

  let projectCIStatusCollection: ProjectCIStatus[] = [];

  for (let project of projects) {
    const pipelines = await project.pipelines.list();
    //only for projects that uses pipelines
    if (pipelines.length != 0) {
      const latestPipeline = pipelines[0].data;
      const element = { name: project.data.name, branch: latestPipeline.ref, status: latestPipeline.status };
      projectCIStatusCollection.push(element);
    }
  }
  return projectCIStatusCollection;
}

let convertToMarkDown = (collection: ProjectCIStatus[]) => {
  return json2md([
    { h1: 'Report' },
    { h2: `created: ${new Date().toLocaleString('se-SE')}` },
    { p: "Results from the last run." },
    {
      table: {
        headers: ['Name', 'Ref', 'Status'],
        rows: collection.map((element) => ({
          Name: element.name,
          Ref: element.branch,
          Status: element.status
        })),
      },
    },
  ]);
}

(async () => {

  if (!options.group) {
    console.error("You need to specify a group!");
    process.exit(0);
  }

  let groupName = program.opts().group;
  let projectCollection: ProjectCIStatus[] = await getProjectCollection(groupName);
  let markdown = convertToMarkDown(projectCollection);
  fs.writeFileSync("ci-report.md", markdown);
})();







